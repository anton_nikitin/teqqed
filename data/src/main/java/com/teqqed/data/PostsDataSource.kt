package com.teqqed.data

import com.teqqed.data.api.TeqqedApi
import com.teqqed.data.models.comments.Comments
import com.teqqed.data.models.posts.Posts
import com.teqqed.data.models.users.Users

class PostsDataSource(private val teqqedApi: TeqqedApi) {

    suspend fun getPosts(): Posts? {
        return try {
            teqqedApi.getPosts()
        } catch(ex: Exception) {
            return null
        }
    }
}