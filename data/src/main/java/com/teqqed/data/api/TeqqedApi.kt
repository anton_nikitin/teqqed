package com.teqqed.data.api

import com.teqqed.data.models.comments.Comments
import com.teqqed.data.models.posts.Posts
import com.teqqed.data.models.users.Users
import retrofit2.http.GET

interface TeqqedApi {

    @GET("posts")
    suspend fun getPosts(): Posts

    @GET("users")
    suspend fun getUsers(): Users

    @GET("comments")
    suspend fun getComments(): Comments
}