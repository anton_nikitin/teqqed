package com.teqqed.data

import com.teqqed.data.api.TeqqedApi
import com.teqqed.data.models.comments.Comments

class CommentsDataSource(private val teqqedApi: TeqqedApi) {
    suspend fun getComments(): Comments? {
        return try {
            teqqedApi.getComments()
        } catch (ex: Exception) {
            return null
        }
    }
}