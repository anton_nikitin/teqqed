package com.teqqed.data.repository

import com.teqqed.data.PostsDataSource
import com.teqqed.domain.PostRepository
import com.teqqed.domain.entities.comments.CommentEntity
import com.teqqed.domain.entities.comments.CommentsEntity
import com.teqqed.domain.entities.posts.PostEntity
import com.teqqed.domain.entities.posts.PostsEntity
import com.teqqed.domain.entities.users.UserEntity
import com.teqqed.domain.entities.users.UsersEntity

class PostsRepositoryImpl(private val postsDataSource: PostsDataSource) : PostRepository {

    override suspend fun getPosts(): PostsEntity {
        val result = PostsEntity()
        result.posts = ArrayList()
        postsDataSource.getPosts()?.forEach { postItem ->
            val postEntity = PostEntity(
                body = postItem.body,
                id = postItem.id,
                title = postItem.title,
                userId = postItem.userId
            )
            result.posts?.add(postEntity)
        }
        return result
    }
}