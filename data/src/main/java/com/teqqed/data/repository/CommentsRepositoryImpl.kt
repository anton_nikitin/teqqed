package com.teqqed.data.repository

import com.teqqed.data.CommentsDataSource
import com.teqqed.domain.CommentsRepository
import com.teqqed.domain.entities.comments.CommentEntity
import com.teqqed.domain.entities.comments.CommentsEntity

class CommentsRepositoryImpl(private val commentsDataSource: CommentsDataSource): CommentsRepository {
    override suspend fun getComments(): CommentsEntity {
        val result = CommentsEntity()
        result.comments = ArrayList()
        commentsDataSource.getComments()?.forEach { commentsItem ->
            val commentEntity = CommentEntity(
                body = commentsItem.body,
                email =  commentsItem.email,
                id = commentsItem.id,
                name = commentsItem.name,
                postId = commentsItem.postId
            )
            result.comments?.add(commentEntity)
        }
        return result
    }
}