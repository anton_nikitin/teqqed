package com.teqqed.data.repository

import com.teqqed.data.UsersDataSource
import com.teqqed.domain.UsersRepository
import com.teqqed.domain.entities.users.UserEntity
import com.teqqed.domain.entities.users.UsersEntity

class UsersRepositoryImpl(private val usersDataSource: UsersDataSource): UsersRepository {
    override suspend fun getUsers(): UsersEntity {
        val result = UsersEntity()
        result.users = ArrayList()
        usersDataSource.getUsers()?.forEach { usersItem ->
            val userEntity = UserEntity(
                id = usersItem.id,
                name = usersItem.name
            )
            result.users?.add(userEntity)
        }
        return result
    }
}