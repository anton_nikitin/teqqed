package com.teqqed.data

import com.teqqed.data.api.TeqqedApi
import com.teqqed.data.models.users.Users

class UsersDataSource(private val teqqedApi: TeqqedApi) {

    suspend fun getUsers(): Users? {
        return try {
            teqqedApi.getUsers()
        } catch (ex: Exception) {
            return null
        }
    }
}