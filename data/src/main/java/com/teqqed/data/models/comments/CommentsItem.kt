package com.teqqed.data.models.comments

data class CommentsItem(
    var body: String? = null,
    var email: String? = null,
    var id: Int? = null,
    var name: String? = null,
    var postId: Int? = null
)