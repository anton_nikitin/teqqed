package com.teqqed.data.models.users

data class Company(
    var bs: String? = null,
    var catchPhrase: String? = null,
    var name: String? = null
)