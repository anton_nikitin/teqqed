package com.teqqed.data.models.users

data class UsersItem(
    var address: Address? = null,
    var company: Company? = null,
    var email: String? = null,
    var id: Int? = null,
    var name: String? = null,
    var phone: String? = null,
    var username: String? = null,
    var website: String? = null
)