package com.teqqed.data.models.users

data class Address(
    var city: String? = null,
    var geo: Geo? = null,
    var street: String? = null,
    var suite: String? = null,
    var zipcode: String? = null
)