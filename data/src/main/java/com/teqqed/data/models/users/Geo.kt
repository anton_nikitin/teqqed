package com.teqqed.data.models.users

data class Geo(
    var lat: String? = null,
    var lng: String? = null
)