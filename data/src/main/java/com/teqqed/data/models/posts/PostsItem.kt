package com.teqqed.data.models.posts

data class PostsItem(
    var body: String?,
    var id: Int?,
    var title: String?,
    var userId: Int?
)