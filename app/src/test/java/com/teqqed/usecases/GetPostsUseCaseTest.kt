package com.teqqed.usecases

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.teqqed.domain.PostRepository
import com.teqqed.domain.entities.posts.PostsEntity
import com.teqqed.domain.usecases.GetPostsUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetPostsUseCaseTest {

    private val mockPostsRepository: PostRepository = mock()
    private val getPostsUseCase = GetPostsUseCase(mockPostsRepository)

    @Test
    fun verifyResultWhenRepoMockReturnSuccessState() {
        runBlocking {
            val result = PostsEntity(arrayListOf())
            given(mockPostsRepository.getPosts()).willReturn(result)

            val expectedResult = PostsEntity(arrayListOf())
            val realResult = getPostsUseCase.execute()

            Assert.assertEquals(expectedResult, realResult)
        }
    }

    @Test
    fun verifyUseCaseCallRepository() {
        runBlocking {
            given(mockPostsRepository.getPosts())
                .willReturn(PostsEntity(arrayListOf()))

            getPostsUseCase.execute()

            verify(mockPostsRepository, times(1)).getPosts()
        }
    }
}