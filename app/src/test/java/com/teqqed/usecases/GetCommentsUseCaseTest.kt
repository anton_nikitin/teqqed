package com.teqqed.usecases

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.teqqed.domain.CommentsRepository
import com.teqqed.domain.entities.comments.CommentsEntity
import com.teqqed.domain.usecases.GetCommentsUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetCommentsUseCaseTest {

    private val mockCommentsRepository: CommentsRepository = mock()
    private val getCommentsUseCase = GetCommentsUseCase(mockCommentsRepository)

    @Test
    fun verifyResultWhenRepoMockReturnSuccessState() {
        runBlocking {
            val result = CommentsEntity(arrayListOf())
            given(mockCommentsRepository.getComments()).willReturn(result)

            val expectedResult = CommentsEntity(arrayListOf())
            val realResult = getCommentsUseCase.execute()

            Assert.assertEquals(expectedResult, realResult)
        }
    }

    @Test
    fun verifyUseCaseCallRepository() {
        runBlocking {
            given(mockCommentsRepository.getComments())
                .willReturn(CommentsEntity(arrayListOf()))

            getCommentsUseCase.execute()

            verify(mockCommentsRepository, times(1)).getComments()
        }
    }

}