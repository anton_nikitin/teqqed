package com.teqqed.usecases

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.teqqed.domain.UsersRepository
import com.teqqed.domain.entities.users.UsersEntity
import com.teqqed.domain.usecases.GetUsersUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetUsersUseCaseTest {

    private val mockUsersRepository: UsersRepository = mock()
    private val getUsersUseCase = GetUsersUseCase(mockUsersRepository)

    @Test
    fun verifyResultWhenRepoMockReturnSuccessState() {
        runBlocking {
            val result = UsersEntity(arrayListOf())
            given(mockUsersRepository.getUsers()).willReturn(result)

            val expectedResult = UsersEntity(arrayListOf())
            val realResult = getUsersUseCase.execute()

            Assert.assertEquals(expectedResult, realResult)
        }
    }

    @Test
    fun verifyFindUserByIdSuccessState() {
        runBlocking {
            val result = UsersEntity(arrayListOf())
            given(mockUsersRepository.getUsers()).willReturn(result)

            val expectedResult = UsersEntity(arrayListOf()).users
            val realResultUsers = getUsersUseCase.execute().users

            realResultUsers?.forEachIndexed { index, userEntity ->
                val currentExpectedResult = expectedResult?.first { it.id == 1 }
                val realResult = userEntity.id

                Assert.assertEquals(realResult, currentExpectedResult)

            }
        }
    }

    @Test
    fun verifyUseCaseCallRepository() {
        runBlocking {
            given(mockUsersRepository.getUsers())
                .willReturn(UsersEntity(arrayListOf()))

            getUsersUseCase.execute()

            verify(mockUsersRepository, times(1)).getUsers()
        }
    }
}