package com.teqqed.core.koin

import com.teqqed.domain.usecases.GetCommentsUseCase
import com.teqqed.domain.usecases.GetPostsUseCase
import com.teqqed.domain.usecases.GetUsersUseCase
import com.teqqed.features.post.detail.presenter.PostCommentsHeader
import com.teqqed.features.post.detail.presenter.PostCommentsPresenter
import com.teqqed.features.post.detail.presenter.PostHeaderPresenter
import com.teqqed.features.post.detail.viewmodel.PostDetailViewModel
import com.teqqed.features.posts.presenters.PostsPresenter
import com.teqqed.features.posts.viewmodel.PostsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val coreModule = module {

    factory { GetPostsUseCase(postRepository = get()) }
    factory { GetUsersUseCase(usersRepository = get()) }
    factory { GetCommentsUseCase(commentsRepository = get()) }

    viewModel {
        PostsViewModel(getPostsUseCase = get())
    }

    viewModel {
        PostDetailViewModel(
            getUsersUseCase = get(),
            getCommentsUseCase = get()
        )
    }

    single {
        listOf(
            PostsPresenter(),
            PostHeaderPresenter(),
            PostCommentsHeader(),
            PostCommentsPresenter()
        )
    }
}