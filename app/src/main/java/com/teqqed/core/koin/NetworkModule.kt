package com.teqqed.core.koin

import com.teqqed.R
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.teqqed.core.extensions.resString
import com.teqqed.data.CommentsDataSource
import com.teqqed.data.PostsDataSource
import com.teqqed.data.UsersDataSource
import com.teqqed.data.api.TeqqedApi
import com.teqqed.data.repository.CommentsRepositoryImpl
import com.teqqed.data.repository.PostsRepositoryImpl
import com.teqqed.data.repository.UsersRepositoryImpl
import com.teqqed.domain.CommentsRepository
import com.teqqed.domain.PostRepository
import com.teqqed.domain.UsersRepository

import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { Dispatcher() }

    single { provideRetrofitInstance() }

    factory { provideApiService(retrofit = get()) }
    factory { PostsDataSource(teqqedApi = get()) }
    factory { UsersDataSource(teqqedApi = get()) }
    factory { CommentsDataSource(teqqedApi = get()) }

    single<PostRepository> {
        PostsRepositoryImpl(
            postsDataSource = get()
        )
    }

    single<UsersRepository> {
        UsersRepositoryImpl(usersDataSource = get()
        )
    }

    single<CommentsRepository> {
        CommentsRepositoryImpl(commentsDataSource = get()
        )
    }
}

private fun provideApiService(retrofit: Retrofit): TeqqedApi =
    retrofit.create(TeqqedApi::class.java)

private fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
    .baseUrl(R.string.base_url.resString)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .client(OkHttpClient.Builder() .addInterceptor(
        HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)).build())
    .build()
