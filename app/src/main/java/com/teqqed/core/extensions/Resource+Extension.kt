package com.teqqed.core.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import androidx.annotation.StringRes
import com.teqqed.core.extensions.ActivityProvider.application
import com.teqqed.core.extensions.ActivityProvider.currentActivity
import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentLinkedQueue

var useCurrentActivityContext: Boolean = true

internal inline val context: ContextWrapper
    get() = if (useCurrentActivityContext) currentActivity ?: application!! else application!!

val @receiver:StringRes Int.resString: String
    get() = context.resources.getString(this)

inline fun <reified T> Int.resString(vararg formatArgs: T): String {
    val context: ContextWrapper = if (useCurrentActivityContext) currentActivity ?: application!! else application!!
    return context.resources!!.getString(this, *formatArgs)
}

internal var _currentActivity: WeakReference<Activity>? = null


object ActivityProvider {

    internal val applicationListeners = ConcurrentLinkedQueue<(Application) -> Unit>()

    @JvmStatic
    val currentActivity: Activity?
        get() {
            return _currentActivity?.get()
        }

    @SuppressLint("StaticFieldLeak")
    private var _appliation: Application? = null
        private set(value) {
            field = value
            if(value != null){
                applicationListeners.forEach {
                    it.invoke(value)
                }
            }
        }

    val application: Application?
        get() = _appliation ?: initAndGetAppCtxWithReflection()

    @SuppressLint("PrivateApi")
    private fun initAndGetAppCtxWithReflection(): Application? {
        // Fallback, should only run once per non default process.
        val activityThread = Class.forName("android.app.ActivityThread")
        val ctx = activityThread.getDeclaredMethod("currentApplication").invoke(null) as? Context
        if (ctx is Application) {
            _appliation = ctx
            return ctx
        }
        return null
    }
}