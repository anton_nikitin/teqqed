package com.teqqed.features.posts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teqqed.domain.entities.posts.PostsEntity
import com.teqqed.domain.usecases.GetPostsUseCase
import kotlinx.coroutines.launch

class PostsViewModel(private val getPostsUseCase: GetPostsUseCase): ViewModel() {

    val postsList: MutableLiveData<PostsEntity> = MutableLiveData()

    init {
        handlePostsList()
    }

    private fun handlePostsList() {
        viewModelScope.launch {
            val postsResults = getPostsUseCase.execute()
            postsList.postValue(postsResults)
        }
    }
}