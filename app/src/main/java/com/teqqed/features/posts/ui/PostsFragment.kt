package com.teqqed.features.posts.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.teqqed.R
import com.teqqed.models.PostItemEntity
import com.teqqed.domain.entities.posts.PostEntity
import com.teqqed.features.posts.presenters.PostsPresenter
import com.teqqed.features.posts.viewmodel.PostsViewModel
import kotlinx.android.synthetic.main.fragment_posts.*
import net.kibotu.android.recyclerviewpresenter.PresenterAdapter
import net.kibotu.android.recyclerviewpresenter.PresenterModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostsFragment: Fragment() {

    private val viewModel: PostsViewModel by viewModel()
    private var adapter = PresenterAdapter()
    private val items = arrayListOf<PresenterModel<PostEntity>>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        observerLiveData()
    }

    private fun setupAdapter() {
        adapter.registerPresenter(PostsPresenter())
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.adapter = adapter
        recycler_view.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        adapter.onItemClick { _, _, position ->
            val postItem = PostItemEntity(
                body = items[position].model.body,
                id = items[position].model.id,
                title = items[position].model.title,
                userId = items[position].model.userId
            )
            findNavController().navigate(R.id.action_posts_to_detail, bundleOf("postEntity" to postItem))
        }
    }

    private fun observerLiveData() {
        viewModel.postsList.observe(viewLifecycleOwner, { postsEntity ->
            if (postsEntity != null) {
                items.clear()
                progress_bar.visibility = View.GONE
                postsEntity.posts?.forEach { postEntity ->
                    items.add(PresenterModel(postEntity, R.layout.item_post))
                }
                adapter.submitList(items)
                adapter.notifyDataSetChanged()
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.unregisterPresenter()
    }
}