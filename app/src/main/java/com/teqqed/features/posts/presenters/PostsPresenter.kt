package com.teqqed.features.posts.presenters

import com.teqqed.R
import androidx.recyclerview.widget.RecyclerView
import com.teqqed.domain.entities.posts.PostEntity
import kotlinx.android.synthetic.main.item_post.view.*
import net.kibotu.android.recyclerviewpresenter.Adapter
import net.kibotu.android.recyclerviewpresenter.Presenter
import net.kibotu.android.recyclerviewpresenter.PresenterModel

class PostsPresenter: Presenter<PostEntity>() {
    override val layout: Int = R.layout.item_post

    override fun bindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        item: PresenterModel<PostEntity>,
        position: Int,
        payloads: MutableList<Any>?,
        adapter: Adapter
    ) = with(viewHolder.itemView) {
        post_title_tv.text = item.model.title
        post_body_tv.text = item.model.body
    }
}