package com.teqqed.features.post.detail.presenter

import com.teqqed.R
import androidx.recyclerview.widget.RecyclerView
import com.teqqed.domain.entities.comments.CommentEntity
import com.teqqed.domain.entities.comments.CommentsEntity
import kotlinx.android.synthetic.main.item_comment.view.*
import net.kibotu.android.recyclerviewpresenter.Adapter
import net.kibotu.android.recyclerviewpresenter.Presenter
import net.kibotu.android.recyclerviewpresenter.PresenterModel

class PostCommentsPresenter: Presenter<CommentEntity>() {
    override val layout: Int = R.layout.item_comment

    override fun bindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        item: PresenterModel<CommentEntity>,
        position: Int,
        payloads: MutableList<Any>?,
        adapter: Adapter
    ) = with(viewHolder.itemView) {
        user_email_tv.text = item.model.email
        comment_body_tv.text = item.model.body
    }
}