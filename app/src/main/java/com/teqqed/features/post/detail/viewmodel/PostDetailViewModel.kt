package com.teqqed.features.post.detail.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teqqed.domain.entities.comments.CommentsEntity
import com.teqqed.domain.usecases.GetCommentsUseCase
import com.teqqed.domain.usecases.GetUsersUseCase
import com.teqqed.models.PostItemEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class PostDetailViewModel(private val getUsersUseCase: GetUsersUseCase,
                          private val getCommentsUseCase: GetCommentsUseCase): ViewModel() {

    val commentsLiveData: MutableLiveData<CommentsEntity> = MutableLiveData()
    val error: MutableLiveData<String> = MutableLiveData()

    fun handlePostDetail(postItemEntity: PostItemEntity?) {
        viewModelScope.launch {
            val usersFlow = flowOf(getUsersUseCase.execute())
            val commentsFlow = flowOf(getCommentsUseCase.execute())

            usersFlow.zip(commentsFlow) { users, comments ->
                postItemEntity?.username =
                    users.users?.first { it.id == postItemEntity?.userId }?.name
                return@zip comments
            }.flowOn(Dispatchers.Default)
                .catch { e ->
                    error.postValue(e.message)
                }.collect {
                    commentsLiveData.postValue(it)
                }
        }
    }
}