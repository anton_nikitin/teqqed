package com.teqqed.features.post.detail.presenter

import androidx.recyclerview.widget.RecyclerView
import com.teqqed.R
import kotlinx.android.synthetic.main.item_comments_header.view.*
import net.kibotu.android.recyclerviewpresenter.Adapter
import net.kibotu.android.recyclerviewpresenter.Presenter
import net.kibotu.android.recyclerviewpresenter.PresenterModel

class PostCommentsHeader: Presenter<String>() {
    override val layout: Int
        get() = R.layout.item_comments_header

    override fun bindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        item: PresenterModel<String>,
        position: Int,
        payloads: MutableList<Any>?,
        adapter: Adapter
    ) = with(viewHolder.itemView) {
        post_comments_count_tv.text = item.model
    }
}