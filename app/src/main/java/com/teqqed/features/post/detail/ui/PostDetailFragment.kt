package com.teqqed.features.post.detail.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.teqqed.R
import com.teqqed.core.extensions.resString
import com.teqqed.core.extensions.toast
import com.teqqed.features.post.detail.presenter.PostCommentsHeader
import com.teqqed.features.post.detail.presenter.PostCommentsPresenter
import com.teqqed.features.post.detail.presenter.PostHeaderPresenter
import com.teqqed.models.PostItemEntity
import com.teqqed.features.post.detail.viewmodel.PostDetailViewModel
import kotlinx.android.synthetic.main.fragment_post_detail.*
import kotlinx.android.synthetic.main.fragment_post_detail.progress_bar
import kotlinx.android.synthetic.main.fragment_post_detail.toolbar
import kotlinx.android.synthetic.main.fragment_posts.*
import net.kibotu.android.recyclerviewpresenter.PresenterAdapter
import net.kibotu.android.recyclerviewpresenter.PresenterModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class PostDetailFragment: Fragment() {

    private val viewModel by viewModel<PostDetailViewModel>()
    private var postItemEntity: PostItemEntity? = null
    private val adapter = PresenterAdapter()
    private val items = arrayListOf<PresenterModel<Any?>>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_post_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postItemEntity = arguments?.getParcelable("postEntity")
        setupToolbar()
        setupAdapter()
        viewModel.handlePostDetail(postItemEntity)
        observerLiveData()
    }

    private fun observerLiveData() {
        viewModel.commentsLiveData.observe(viewLifecycleOwner, {
            if (it != null) {
                progress_bar.visibility = View.GONE
                items.add(PresenterModel(postItemEntity, R.layout.item_post_header))
                items.add(PresenterModel(String.format(R.string.comments_count.resString, it.comments?.filter { it.postId == postItemEntity?.id }?.size), R.layout.item_comments_header))
                it.comments?.filter { it.postId == postItemEntity?.id }?.forEach { commentEntity ->
                    items.add(PresenterModel(commentEntity, R.layout.item_comment))
                }
                adapter.submitList(items)
                adapter.notifyDataSetChanged()
            }
        })

        viewModel.error.observe(viewLifecycleOwner, {
            progress_bar.isVisible = it.isNullOrEmpty()
            requireContext().toast(R.string.network_error.resString)
        })
    }

    private fun setupAdapter() {
        adapter.registerPresenter(PostHeaderPresenter())
        adapter.registerPresenter(PostCommentsHeader())
        adapter.registerPresenter(PostCommentsPresenter())
        post_detail_rv.layoutManager = LinearLayoutManager(requireContext())
        post_detail_rv.adapter = adapter
    }

    private fun setupToolbar() {
        toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter.unregisterPresenter()
    }
}