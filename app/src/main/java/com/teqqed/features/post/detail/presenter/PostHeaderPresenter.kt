package com.teqqed.features.post.detail.presenter

import com.teqqed.R
import androidx.recyclerview.widget.RecyclerView
import com.teqqed.models.PostItemEntity
import kotlinx.android.synthetic.main.item_post_header.view.*
import net.kibotu.android.recyclerviewpresenter.Adapter
import net.kibotu.android.recyclerviewpresenter.Presenter
import net.kibotu.android.recyclerviewpresenter.PresenterModel

class PostHeaderPresenter: Presenter<PostItemEntity>() {

    override val layout: Int = R.layout.item_post_header

    override fun bindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        item: PresenterModel<PostItemEntity>,
        position: Int,
        payloads: MutableList<Any>?,
        adapter: Adapter
    ) = with(viewHolder.itemView) {
        post_title_tv.text = item.model.title
        post_body_tv.text = item.model.body
        post_user_tv.text = item.model.username
    }
}