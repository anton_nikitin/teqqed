package com.teqqed.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostItemEntity(
    var body: String? = null,
    var id: Int? = null,
    var title: String? = null,
    var userId: Int? = null,
    var username: String? = null
): Parcelable