package com.teqqed.domain.usecases

import com.teqqed.domain.UsersRepository
import com.teqqed.domain.entities.users.UsersEntity

class GetUsersUseCase(private val usersRepository: UsersRepository) {

    suspend fun execute(): UsersEntity {
        return usersRepository.getUsers()
    }
}