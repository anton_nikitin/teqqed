package com.teqqed.domain.usecases

import com.teqqed.domain.CommentsRepository
import com.teqqed.domain.entities.comments.CommentsEntity

class GetCommentsUseCase(private val commentsRepository: CommentsRepository) {

    suspend fun execute(): CommentsEntity {
        return commentsRepository.getComments()
    }
}