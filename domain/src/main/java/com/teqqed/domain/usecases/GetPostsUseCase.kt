package com.teqqed.domain.usecases

import com.teqqed.domain.PostRepository
import com.teqqed.domain.entities.posts.PostsEntity

class GetPostsUseCase(private val postRepository: PostRepository) {

    suspend fun execute(): PostsEntity {
        return postRepository.getPosts()
    }
}