package com.teqqed.domain

import com.teqqed.domain.entities.users.UsersEntity

interface UsersRepository {
    suspend fun getUsers(): UsersEntity
}