package com.teqqed.domain.entities.posts

data class PostsEntity(
    var posts: ArrayList<PostEntity>? = null
)