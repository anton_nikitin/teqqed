package com.teqqed.domain.entities.posts

class PostEntity(
    var body: String?,
    var id: Int?,
    var title: String?,
    var userId: Int?
)