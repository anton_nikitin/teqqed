package com.teqqed.domain.entities.users

data class UsersEntity(
    var users: ArrayList<UserEntity>? = null
)