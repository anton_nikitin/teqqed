package com.teqqed.domain.entities.comments

class CommentEntity(
    var body: String?,
    var email: String?,
    var id: Int?,
    var name: String?,
    var postId: Int?
)