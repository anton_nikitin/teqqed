package com.teqqed.domain.entities.users

class UserEntity(
    var id: Int? = null,
    var name: String? = null
)