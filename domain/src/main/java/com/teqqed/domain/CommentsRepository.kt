package com.teqqed.domain

import com.teqqed.domain.entities.comments.CommentsEntity

interface CommentsRepository {
    suspend fun getComments(): CommentsEntity
}