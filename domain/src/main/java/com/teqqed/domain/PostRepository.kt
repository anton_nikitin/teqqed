package com.teqqed.domain

import com.teqqed.domain.entities.posts.PostsEntity

interface PostRepository {
    suspend fun getPosts(): PostsEntity
}