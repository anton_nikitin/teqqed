# Teqqed code challenge  [![API](https://img.shields.io/badge/API-15%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=24) [![Gradle Version](https://img.shields.io/badge/gradle-6.1.1-green.svg)](https://docs.gradle.org/current/release-notes) [![Kotlin](https://img.shields.io/badge/kotlin-1.4.20-green.svg)](https://github.com/JetBrains/kotlin) [![Android Studio Canary](https://img.shields.io/badge/Android%20Studio%20Canary-latest-green.svg)](https://developer.android.com/studio/preview/)

* [DEVELOPMENT](#development)
	+ [How to build](#how-to-build)
	+ [Project Structure](#project-structure)
	
## DEVELOPMENT
	
### How to build

#### Full build with Android Studio

1. The first time you open the project, you should sync with gradle.
2. connect device or launch android emulator
3. Hit the build button

#### Full build on command line

    gradlew clean build
	
### Project Structure
	
	1.I'm using clean architecture for the project and MVVM pattern.

![Alt text](https://i0.wp.com/apptractor.ru/wp-content/uploads/2018/03/1-11.png?w=647&ssl=1)
![Alt text](https://camo.githubusercontent.com/73a6089c5405c9fcf7cca999d470ada7629ad8a5/68747470733a2f2f64726976652e676f6f676c652e636f6d2f75633f69643d314b304441794b4443495369347433363168336c474e4a33647a4846764653464a)


## Contributors

[Anton Nikitin](nikitin.k26@gmail.com)